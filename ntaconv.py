from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import csv
import numpy as np

def readPolyData(fn):
    polyArr = []
    
    f = open(fn, 'rU')
    csvr = csv.reader(f, delimiter=',', quotechar='|')
    
    fA  = []
    dct = {}
    ct = 0
    for ln in csvr:
        tA = []
        i = 0
        while i < (len(ln)-7):
            tA.append((float(ln[i].strip()), float(ln[i+1].strip())))
            i += 4
        polyArr.append(Polygon(tA))
        dct[ct] = ln[-4]
        ct += 1
    return (polyArr, dct)
    
def whichPoly(tp, p):
    polyArr = tp[0]
    dct = tp[1]
    for i in range(len(polyArr)):
        if(polyArr[i].contains(p)):
            return (dct[i])
    return -1

#print whichPoly(readPolyData("../nynta.csv"), Point(-74.010461123, 40.709158123))
#-73.99236511, 40.68969838
