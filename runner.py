import pickle
import csv
import datetime
import numpy as np

xd = pickle.load(open("mta_dict.pkl", "rb"))

x = []
y = []

for i in xd:
    x.append([i[0], i[1]])
    y.append(xd[i])

f = open("../weather2.csv", "rU")
csvr = csv.reader(f, delimiter=',', quotechar='|')

wd = {}

for line in csvr:
    if(line[0] != "date"):
        l = line[0].split("/")
        date = '%0*d' % (2, int(l[0])) + "/" + '%0*d' % (2, int(l[1])) + "/20" + l[2]
        wd[date] = line[1:4]

for i in range(len(x)):
    x[i] += wd[x[i][0]]
    dt = x[i][0].split("/")
    x[i].append(datetime.date(int(dt[-1]), int(dt[0]), int(dt[1])).isocalendar()[1])
    x[i].append(0 if datetime.date(int(dt[-1]), int(dt[0]), int(dt[1])).weekday() >= 5 else 1)
    x[i].append(1 if datetime.date(int(dt[-1]), int(dt[0]), int(dt[1])).weekday() >= 5 else 0)

dd = pickle.load(open("demographics.pkl", "rb"))

i = 0

while(i < len(x)):
    if(x[i][1] in dd):
        x[i] += dd[x[i][1]]
        i += 1
    else:
        x = x[0:i] + x[i+1:]
        y = y[0:i] + y[i+1:]

for i in range(len(x)):
    x[i] = x[i][0:-5] + np.ndarray.tolist(x[i][-5]) + [x[i][-4]] + np.ndarray.tolist(x[i][-3]) + x[i][-2:]

for i in range(len(x)):
    x[i] = x[i][1:]

def hsh(s):
    st = 0
    for i in range(len(s)):
        st += 10**(i*2)*ord(s[i])
    return st
    
for i in range(len(x)):
    x[i][0] = hsh(x[i][0])
    for j in range(len(x[i])):
        x[i][j] = float(x[i][j])

print(len(x))

print(len(y))

def save_object(obj, filename):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

for i in range(len(x)):
    x[i][0] /= 10000000
    s = 0
    for j in range(7, 7+14):
        s += x[i][j]
    for j in range(7, 7+14):
        x[i][j]/=s 
        x[i][j] *= 100
    s = 0
    for j in range(22, 22+10):
        s += x[i][j]
    for j in range(22, 22+10):
        x[i][j]/=s
        x[i][j]*=100
    x[i][32]/=500
    x[i][33]/=500

print(len(x[0]))

print(max(x[49]))

save_object(x, "xMTA.pkl")
save_object(y, "yMTA.pkl")


