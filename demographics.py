import pandas as pd

def demographics():
    demos = pd.read_csv('demographics.csv')
    demos_vals = demos.values
    demos_dict = {}
    for i in range(len(demos_vals)):
        vals = demos_vals[i]
        demos_dict[vals[2]] = [vals[4], vals[5], vals[8], vals[9], vals[10]]
    return demos_dict
