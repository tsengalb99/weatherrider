from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import numpy as np
import ntaconv as nc
import csv
import pickle

f = open("../mta_trips.csv", "rU")
csvr = csv.reader(f, delimiter=',', quotechar='|')

mta_dict = {}

tup = nc.readPolyData("../nynta.csv")

def save_object(obj, filename):
    with open(filename, 'wb') as output:
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

ct = 0
lf = 0
nta = "asdf"
for line in csvr:
    ct += 1
    if(ct%1000 == 0):
        print ct
    if(line[-1] != 'longitude'):
        if( lf != float(line[-1])):
            lf = float(line[-1])
            nta = nc.whichPoly(tup, Point(float(line[-1]), float(line[-2])))
        else:
            if((line[5].split()[0],nta) in mta_dict):
                mta_dict[(line[5].split()[0], nta)] += int(line[-4])
            else:
                mta_dict[(line[5].split()[0], nta)] = int(line[-4])

save_object(mta_dict, "mta_dict.pkl")
print mta_dict

