from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.ensemble import RandomForestRegressor
import pickle
import numpy as np
import random

x = pickle.load(open("xMTA.pkl", "rb"))
y = pickle.load(open("yMTA.pkl", "rb"))

combined = list(zip(x, y))
random.shuffle(combined)

x[:], y[:] = zip(*combined)

xTrain = x[0:30000]
yTrain = y[0:30000]

xTest = x[30000:]
yTest = y[30000:]

linreg = linear_model.LinearRegression()
linreg.fit(xTest, yTest)
yPredLR = linreg.predict(xTest)

print(mean_squared_error(yPredLR, yTest))
print(r2_score(yPredLR, yTest))

rf = RandomForestRegressor(max_depth=20, n_estimators=30)
rf.fit(xTest, yTest)
yPredRF = rf.predict(xTest)
print(mean_squared_error(yPredRF, yTest))
print(r2_score(yPredRF, yTest))



