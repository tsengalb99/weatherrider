Algorithm to predict number of users for Uber, taxi, and subway data given weather, location, and day.

Achieved <1% error on average, and correlation between predicted and actual number of users r^2 > 0.995

trainer.py is the main training file

datasets are private and not releasable